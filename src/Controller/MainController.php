<?php

namespace App\Controller;

use DateTime;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(HttpClientInterface $client, Request $request): Response
    {
        $apiKey = 'db988691faf182dfc3750cd1e57f3718';
        //Je vérifie que le formulair a été soumis
        if ($request->query->get('city')) {
            //Je récupère la ville ou le pays
            $city = $request->query->get('city');
            //Je récupère l'API pour récupérer les informations de longitude et latitude
            $responseCity = $client->request('GET', 'http://api.openweathermap.org/geo/1.0/direct?q=' . $city . '&limit=1&appid=' . $apiKey);
            $data = $responseCity->toArray();
            //Gestion d'erreur
            if (empty($data)) {
                $this->addFlash('danger', 'Ville non trouvée');
                return $this->redirectToRoute('app_home');
            }
            //Je récupère la latitude et la longitude
            $lat = $data[0]['lat'];
            $lon = $data[0]['lon'];
            //Je récupère les données de l'API pour récupérer les informations de la météo
            $responseDetails = $client->request('GET', 'https://api.openweathermap.org/data/2.5/onecall?lat=' . $lat . '&lon=' . $lon . '&units=metric&lang=fr&appid=' . $apiKey);
            $dataDetails = $responseDetails->toArray();
            $weathers = $dataDetails['daily'];

            //Je récupère les icones afin de les encoder pour les afficher
            $icons = [];
            $dates = [];
            foreach ($weathers as $key => $value) {
                $pic =  utf8_encode('https://openweathermap.org/img/wn/' . $value['weather'][0]['icon'] . '@4x.png');
                $icons[$key] = $pic;
                //Manipulation de la date pour avoir un format plus approprié
                $date = new DateTime('now');
                $dateCurent = strftime("%A %d %B", strtotime($date->format('D j F')));
                $date->modify('+' . ($key + 1) . ' day');
                setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');
                date_default_timezone_set('Europe/Paris');
                $newDate = $date->format('D j F');
                $dates[$key] = strftime("%A %d %B", strtotime($newDate));
            }
            // je génére un code couleur aléatoire pour le background dynamique, je n'ai pas trouvé ou se trouve les images.
            $color = dechex(mt_rand(0, 16777215));
            $color = str_pad($color, 6, '0');

            return $this->render('main/detail.html.twig', [
                'weathers' => $weathers,
                'icons' => $icons,
                'bgColor' => $color,
                'city' => $city,
                'dates' => $dates,
                'dateCurent' => $dateCurent,
            ]);
        }
        return $this->render('main/index.html.twig');
    }
    /**
     * @Route("api/weather-informations", name="weather-informations", methods={"GET"})
     */
    public function listeProduitsFavories(Request $request, HttpClientInterface $client, CacheInterface $cache)
    {

        //instanciation de l'adaptateur du cache.
        $cache = new FilesystemAdapter();

        // vérification de l'existence du cache sino on le crée
        $informationsCache = $cache->get('informations', function () use ($client, $request) {
            $apiKey = 'db988691faf182dfc3750cd1e57f3718';
            $city = $request->query->get('city');
            $informations = [];

            $responseCity = $client->request('GET', 'http://api.openweathermap.org/geo/1.0/direct?q=' . $city . '&limit=1&appid=' . $apiKey);
            $data = $responseCity->toArray();
            //Gestion d'erreur
            if (empty($data)) {
                $response = $this->json('Ville non trouvée', 404);
                return $response;
            }
            //Je récupère la latitude et la longitude
            $lat = $data[0]['lat'];
            $lon = $data[0]['lon'];
            //Je récupère les données de l'API pour récupérer les informations de la météo
            $responseDetails = $client->request('GET', 'https://api.openweathermap.org/data/2.5/onecall?lat=' . $lat . '&lon=' . $lon . '&units=metric&lang=fr&appid=' . $apiKey);
            $dataDetails = $responseDetails->toArray();
            $weathers = $dataDetails['daily'];
            foreach ($weathers as $key => $value) {
                $date = new DateTime('now');
                if ($key != 0) {
                    $date->modify('+' . ($key + 1) . ' day');
                }
                setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8');
                date_default_timezone_set('Europe/Paris');
                $newDate = $date->format('D j F');
                $informations[$key] =  [
                    "icon" => 'https://openweathermap.org/img/wn/' . $value['weather'][0]['icon'] . '@4x.png',
                    "day-emperature" => $value['temp']['day'],
                    "night-temperature" => $value['temp']['night'],
                    "humidity" => $value['humidity'],
                    "pressure" => $value['pressure'],
                    "wind-speed" => $value['wind_speed'],
                    "date" => strftime("%A %d %B", strtotime($newDate)),
                ];
            }

            return $informations;
        });

        return $this->json($informationsCache, 200,);
    }
}