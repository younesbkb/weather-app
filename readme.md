# Projet : weather app !

# Installation

```
# On clone le dépot
git clone https://gitlab.com/younesbkb/weather-app.git
# On va dans le dossier
cd weather-app
# On installe les dépendances avec Composer
composer install
# On lance son éditeur favoris
code .
# On lance le serveur interne de Symfony
symfony server:start
```

# Quels sont les principes que tu as appliqué

J'ai travailler sur une version 5.4 de Symfony car c'est la dernièrre LTS (longue time support)
J'ai utilisé le model MVC

# Peux-tu expliquer les décisions que tu as prise et pourquoi c'est la meilleure approche

Mes décisions on été guidé par le temps, j'ai utiliser toutes les ressources que propose Symfony pour pouvoir faire au mieux l'exercice.

# Quelles sont tes recommandations pour un travail futur

Si quelqu'un devait reprendre mon code je pense qu'il faudrait créer des service, pour eviter de réecrire plusieurs fois le même code
